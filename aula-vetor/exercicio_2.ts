//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada
namespace exercicio_2 
{
  let frutas: string[] = ["Maça", "Banana", "Melancia"];
  let i = 0;
  while (i < frutas.length) {
    console.log(frutas[i]);
    i++;
  }
}
