//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.
namespace exercico_3
{
    let livros = [
        { titulo: 'It: A coisa', autor: 'Stephen King' },
        { titulo: 'Diário de um Banana', autor: ' Jeff Kinney' },
        { titulo: 'Mitologia Nórdica', autor: 'Neil Gaiman' },
        { titulo: 'Gears of war: o fim da coalizão', autor: 'Karen Traviss' }
      ];
      
      let titulos = livros.map(function(livro) {
        return livro.titulo;
      });
      let autor = livros.map(function(livro){
        return livro.titulo;
      });
      console.log(titulos);
      console.log(autor);

      let titulo: any[] = [
        {titulo: "It: A coisa", autor: "autor3"},
        {titulo: "Diário de um Banana", autor: "autor 4"},
        {titulo: "Mitologia Nórdica", autor: "autor 5"},
        {titulo: "Gears of war: o fim da coalizão", autor: "autor 6"}
    ];
    
    let livrosAutor3 = livros.filter((livro) => {
        return livro.autor === "autor 3"
    })
    console.log(livrosAutor3);
    let titulosAutor3 = livrosAutor3.map((livro) => {
        return livro.titulo;
    })
    console.log(titulosAutor3);
}

