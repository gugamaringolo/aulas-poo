interface aluno {
    nome: string;
    idade: number;
    notas: number[];
}

namespace exercicio_6
{
    const alunos: aluno[] = [
        {nome: "aluno 1", idade: 20, notas:[8,7,8]},
        {nome: "aluno 2", idade: 23, notas:[7,5,10]},
        {nome: "aluno 3", idade: 49, notas:[10,9,4]},
        {nome: "aluno 4", idade: 18, notas:[7,6,7]},
        {nome: "aluno 5", idade: 33, notas:[10,10,10]}
    ];

    alunos.forEach((aluno) => {
        //let media = aluno.notas.reduce((total, nota) => {return total + nota}) / aluno.notas.length;

        let media = aluno.notas.reduce((total, nota) => {return total + nota}) / aluno.notas.length;

        if (media >= 7){
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está aprovado`);
        }
        else {
            console.log(`A média do aluno: ${aluno.nome} é igual ${media} e está reprovado`);
        }
    });
}