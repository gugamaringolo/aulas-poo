//Escreva um programa TypeScript que inprima todos os números primos de 1 a 50 usando a função

namespace exercicio1
{
    let primos: number = 2;

    while(primos <= 53)
    {
       if  ( primos == 2 || primos == 3 || primos == 5 || primos == 7)
       {
            console.log(`${primos}`);
       }

       if(primos!= 1 && primos % 2 != 0 && primos % 3 != 0 && primos % 5 != 0 && primos % 7 != 0)
       {
            console.log(`${primos}`);
       }
       primos++;
    }
  
}


