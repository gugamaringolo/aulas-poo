//Crie uma função que receba um array de números como parâmetro e retorne a soma desses números.

namespace exercicio3
{
    function somaArray(numeros:number[]){
        let soma:number = 0;
        for(let i = 0 ; i < numeros.length; i++){
            soma = soma + numeros[i]
        }
        return soma;
    }
    console.log(somaArray([1, 2, 3, 4]));
}
