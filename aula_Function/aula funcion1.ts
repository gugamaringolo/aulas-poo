namespace aulaFuncion1
{
    function saudacao(nome?:string){
        if (nome){
            console.log(`olá, ${nome}`);
        }else{
            console.log("olá, estranho!");
        }
    }
    saudacao("Gustavo");

    function pontencia(base:number, expoente: number = 2){
        console.log(Math.pow(base, expoente));
    }
    pontencia(2);
    pontencia(2, 3);
}