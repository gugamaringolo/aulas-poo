namespace function2
{
    function multiplicarpor(n:number):(x:number)=> number{
        return function(x:number):number{
            return x * n;
        }
    }
    let duplicar = multiplicarpor(2);
    let resultado = duplicar(5);
    console.log(resultado);
    let triplicar = multiplicarpor(3);
    resultado = triplicar(5);
    console.log(resultado);
}