//Crie uma função que receba dois números como parâmetros e retorne o menor entre eles.

namespace execicio2
{
    function menor(numero1:number, numero2:number){
        if (numero1 < numero2){
            return numero1;
        }
        else{
            return numero2;
        }
    }
    console.log(`o menor número é:`, menor(12, 6));
}