//Crie uma função que receba uma string como parâmetro e retorne o número de caracteres da string.

namespace execicio1
{
    function tudo(flamengo:string){
        let done:number = 0;
        for (let i = 0; i < flamengo.length; i++) {
            done++;
        }
        return done;
    }

    console.log(tudo("flamengo"));
}

