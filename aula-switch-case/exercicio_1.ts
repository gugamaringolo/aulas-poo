// Escreva um programa que pergunte ao usuário qual o seu nível de conhecimento em TypeScript e exiba uma mensagem de acordo com o nível escolhido:

namespace exercicio_1
{
    let nivel_de_experiência : number;
    nivel_de_experiência = 3;
    
    switch(nivel_de_experiência)
    {
    case 1: console.log("iniciante");
            break;
    case 2: console.log("amador");
            break;
    case 3: console.log("mestre dos mestres");
            break;
    default: console.log("igual o mundia do palmeiras");
    }
}
